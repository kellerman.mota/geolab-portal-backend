package br.com.portal.service.impl;

import br.com.portal.fachada.dao.DAO;
import br.com.portal.fachada.servico.impl.ServicoGenerico;
import br.com.portal.modelo.Configuracao;
import br.com.portal.persistencia.ConfiguracaoDAO;
import br.com.portal.service.ConfiguracaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfiguracaoServiceImpl extends ServicoGenerico<Long, Configuracao> implements ConfiguracaoService {

    @Autowired
    private ConfiguracaoDAO dao;

    @Override
    public void salvar(List<Configuracao> lista) {

        List<Configuracao> remover = listar();

        remover.stream().forEach(e -> this.excluir(e));

        super.salvar(lista);
    }

    @Override
    public DAO<Long, Configuracao> getDAO() {
        return dao;
    }

}
