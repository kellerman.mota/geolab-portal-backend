package br.com.portal.service.impl;

import br.com.portal.fachada.excecoes.ServicoException;
import br.com.portal.fachada.rest.paginacao.OperacaoEnum;
import br.com.portal.fachada.rest.paginacao.Ordenacao;
import br.com.portal.fachada.rest.paginacao.Pagina;
import br.com.portal.fachada.rest.paginacao.Paginacao;
import br.com.portal.fachada.servico.impl.ServicoGenerico;
import br.com.portal.modelo.Enumerator.StatusEnum;
import br.com.portal.modelo.Enumerator.StatusIntegracaoEnum;
import br.com.portal.modelo.Enumerator.StatusTituloEnum;
import br.com.portal.modelo.Titulo;
import br.com.portal.modelo.UsuarioCliente;
import br.com.portal.modelo.dto.DataDTO;
import br.com.portal.modelo.dto.TituloDTO;
import br.com.portal.modelo.dto.UsuarioClienteDTO;
import br.com.portal.modelo.dto.integracao.MensagemIntegracaoDTO;
import br.com.portal.modelo.dto.integracao.TituloIntegracaoDTO;
import br.com.portal.persistencia.TituloDAO;
import br.com.portal.persistencia.UsuarioClienteDAO;
import br.com.portal.service.TituloService;
import br.com.portal.service.UsuarioClienteService;
import br.com.portal.util.bundle.MessageSupport;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class TituloServiceImpl extends ServicoGenerico<Long, Titulo> implements TituloService {

    @Autowired
    private TituloDAO dao;

    @Autowired
    private UsuarioClienteDAO usuarioClienteDAO;

    @Override
    public List<MensagemIntegracaoDTO> salvarTitulos(List<TituloIntegracaoDTO> titulos) {

        List<MensagemIntegracaoDTO> mensagens = new ArrayList<>();

        titulos.stream().forEach(u -> {

            MensagemIntegracaoDTO mensagem = new MensagemIntegracaoDTO();

            mensagem.setCodigo(u.getNumeroNotaFiscal());

            Titulo titulo = preencherTitulo(u);

            try {

                this.salvar(titulo);

                mensagem.setMensagem(MessageSupport.getMessage("MSGS001"));

                mensagem.setStatus(StatusIntegracaoEnum.OK);

                mensagens.add(mensagem);

            } catch (ServicoException se) {

                mensagem.setMensagem(se.getMessage());

                mensagem.setStatus(StatusIntegracaoEnum.NOK);

                mensagens.add(mensagem);
            }

        });

        return mensagens;
    }

    private Titulo preencherTitulo(TituloIntegracaoDTO tituloDTO) {

        Titulo titulo = new Titulo();

        if (StringUtils.isNotBlank(tituloDTO.getCodigoCliente())) {

            UsuarioCliente usuarioCliente = usuarioClienteDAO.findByCodigoSAP(tituloDTO.getCodigoCliente());

            titulo.setCliente(usuarioCliente);
        }

        titulo.setDataEmissao(tituloDTO.getDataEmissao());

        titulo.setDataVencimento(tituloDTO.getDataVencimento());

        titulo.setNumeroNotaFiscal(tituloDTO.getNumeroNotaFiscal());

        titulo.setValorNominal(tituloDTO.getValorNominal());

        titulo.setNumeroDocumento(tituloDTO.getNumeroDocumento());

        titulo.setParcela(tituloDTO.getParcela());

        return titulo;
    }

    @Override
    public Pagina<TituloDTO> listarPaginadoDTO(final Paginacao<Titulo> paginacao) {

        paginacao.setOrdenacao(getOrdenacaoNumeroNotaFiscal());

        Titulo filter = paginacao.getEntidade();

        boolean isBetweenEmissao = isBetweenEmissao(filter);

        boolean isBetweenVencimento = isBetweenVencimento(filter);

        boolean isBetweenValorNominal = isBetweenValorNominal(filter);

        boolean isNumeroNotaFiscal = StringUtils.isNotBlank(filter.getNumeroNotaFiscalFiltro());

        boolean isStatusTitulo = Objects.nonNull(filter.getStatusTitulo());

        Page<Titulo> dados = getDAO().findAll(new Specification<Titulo>() {

                @Override
                public Predicate toPredicate(Root<Titulo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                    List<Predicate> predicates = new ArrayList<>();

                    Date hoje = new Date();

                    if (isBetweenEmissao) {
                        predicates.add(cb.between(root.get("dataEmissao"), filter.getEmissaoInicial(), filter.getEmissaoFinal()));
                    }

                    if (isBetweenVencimento) {
                        predicates.add(cb.between(root.get("dataVencimento"), filter.getVencimentoInicial(), filter.getVencimentoFinal()));
                    }

                    if (isBetweenValorNominal) {
                        predicates.add(cb.between(root.get("valorNominal"), filter.getValorInicial(), filter.getValorFinal()));
                    }

                    if (isNumeroNotaFiscal) {
                        predicates.add(cb.equal(root.get("numeroNotaFiscal"), filter.getNumeroNotaFiscalFiltro()));
                    }

                    if (isStatusTitulo && filter.getStatusTitulo().equals(StatusTituloEnum.VENCIDO)) {

                        predicates.add(cb.lessThan(root.get("dataVencimento"),hoje));

                    }else if (isStatusTitulo){

                        predicates.add(cb.greaterThanOrEqualTo(root.get("dataVencimento"),hoje));

                    }

                    Path joinClienteID =  root.join("cliente").get("id");

                    predicates.add(cb.equal(joinClienteID, filter.getIdClienteFiltro()));

                    return cb.and(predicates.toArray(new Predicate[0]));
                }
            }, paginacao);

        return this.pageToPagina(dados);

    }

    private Ordenacao getOrdenacaoNumeroNotaFiscal() {
        Ordenacao ordenacao = new Ordenacao();
        ordenacao.setCampo("numeroNotaFiscal");
        ordenacao.setOrdenacao(Sort.Direction.DESC);
        return ordenacao;
    }

    private boolean isBetweenValorNominal(Titulo filter) {

        if (Objects.isNull(filter.getValorInicial()) && Objects.isNull(filter.getValorFinal())) {
            return false;
        }

        if (Objects.isNull(filter.getValorInicial())) {
            throw new ServicoException(MessageSupport.getMessage("faixa.valor.incompleto"));

        }

        if (Objects.isNull(filter.getValorFinal())) {
            throw new ServicoException(MessageSupport.getMessage("faixa.valor.incompleto"));

        }

        if (filter.getValorInicial().doubleValue() > filter.getValorFinal().doubleValue()) {
            throw new ServicoException(MessageSupport.getMessage("faixa.valor.erro"));
        }

        return true;
    }

    private boolean isBetweenVencimento(Titulo filter) {
        if (Objects.isNull(filter.getVencimentoInicial()) && Objects.isNull(filter.getVencimentoFinal())) {
            return false;
        }

        if (Objects.isNull(filter.getVencimentoInicial())) {
            throw new ServicoException(MessageSupport.getMessage("periodo.vencimento.incompleto"));

        }

        if (Objects.isNull(filter.getVencimentoFinal())) {
            throw new ServicoException(MessageSupport.getMessage("periodo.vencimento.incompleto"));

        }

        if (filter.getVencimentoFinal().before(filter.getVencimentoInicial())) {
            throw new ServicoException(MessageSupport.getMessage("MSGE007"));
        }

        return true;
    }

    private boolean isBetweenEmissao(Titulo filter) {

        if (Objects.isNull(filter.getEmissaoInicial()) && Objects.isNull(filter.getEmissaoFinal())) {
            return false;
        }

        if (Objects.isNull(filter.getEmissaoInicial())) {
            throw new ServicoException(MessageSupport.getMessage("periodo.emissao.incompleto"));

        }

        if (Objects.isNull(filter.getEmissaoFinal())) {
            throw new ServicoException(MessageSupport.getMessage("periodo.emissao.incompleto"));

        }

        if (filter.getEmissaoFinal().before(filter.getEmissaoInicial())) {
            throw new ServicoException(MessageSupport.getMessage("MSGE007"));
        }

        return true;
    }

    @Override
    public Class<TituloDTO> getDTO() {
        return TituloDTO.class;
    }

    @Override
    public TituloDAO getDAO() {
        return dao;
    }

}
