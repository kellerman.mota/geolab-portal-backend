package br.com.portal.service;


import br.com.portal.fachada.service.Servico;
import br.com.portal.modelo.UsuarioCliente;
import br.com.portal.modelo.dto.integracao.ClienteIntegracaoDTO;
import br.com.portal.modelo.dto.integracao.MensagemIntegracaoDTO;

import java.util.List;

public interface UsuarioClienteService extends Servico<Long, UsuarioCliente> {

    List<MensagemIntegracaoDTO> salvarClientes(List<ClienteIntegracaoDTO> clientes);
}
