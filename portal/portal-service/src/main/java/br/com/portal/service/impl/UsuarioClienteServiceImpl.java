package br.com.portal.service.impl;

import br.com.portal.fachada.excecoes.ServicoException;
import br.com.portal.fachada.rest.paginacao.OperacaoEnum;
import br.com.portal.fachada.rest.paginacao.Pagina;
import br.com.portal.fachada.rest.paginacao.Paginacao;
import br.com.portal.fachada.servico.impl.ServicoGenerico;
import br.com.portal.modelo.Enumerator.StatusEnum;
import br.com.portal.modelo.Enumerator.StatusIntegracaoEnum;
import br.com.portal.modelo.UsuarioCliente;
import br.com.portal.modelo.dto.UsuarioClienteDTO;
import br.com.portal.modelo.dto.integracao.ClienteIntegracaoDTO;
import br.com.portal.modelo.dto.integracao.MensagemIntegracaoDTO;
import br.com.portal.persistencia.UsuarioClienteDAO;
import br.com.portal.service.UsuarioClienteService;
import br.com.portal.util.bundle.MessageSupport;
import br.com.portal.util.encrypt.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UsuarioClienteServiceImpl extends ServicoGenerico<Long, UsuarioCliente> implements UsuarioClienteService {

    @Autowired
    private UsuarioClienteDAO dao;


    @Override
    public List<MensagemIntegracaoDTO> salvarClientes(List<ClienteIntegracaoDTO> clientes) {

        List<MensagemIntegracaoDTO> mensagens = new ArrayList<>();

        clientes.stream().forEach(u -> {

            UsuarioCliente cliente = preencherCliente(u);

            MensagemIntegracaoDTO mensagem = new MensagemIntegracaoDTO();

            try {

                mensagem.setCodigo(u.getCodigoSAP());

                this.salvar(cliente);

                mensagem.setMensagem(MessageSupport.getMessage("MSGS001"));

                mensagem.setStatus(StatusIntegracaoEnum.OK);

            } catch (ServicoException se) {

                mensagem.setMensagem(se.getMessage());

                mensagem.setStatus(StatusIntegracaoEnum.NOK);
            }

            mensagens.add(mensagem);

        });

        return mensagens;
    }

    @Override
    public void preSalvar(UsuarioCliente entidade) {

        super.preSalvar(entidade);

        UsuarioCliente clienteBD = getDAO().findByCodigoSAP(entidade.getCodigoSAP());

        if (Objects.nonNull(clienteBD)) {

            throw new ServicoException(MessageSupport.getMessage("cliente.codigosap.existe"));

        }

        clienteBD = getDAO().findByCnpj(entidade.getCnpj());

        if (Objects.nonNull(clienteBD)) {

            throw new ServicoException(MessageSupport.getMessage("cliente.cnpj.existe"));

        }

    }

    private UsuarioCliente preencherCliente(ClienteIntegracaoDTO u) {

        UsuarioCliente cliente = new UsuarioCliente();

        String cnpj = null;

        if (StringUtils.isNotBlank(u.getCnpj()) && u.getCnpj().length() > 7) {

            cnpj = u.getCnpj().replaceAll("\\.", "").replaceAll("-", "").replaceAll("/", "");

            cliente.setAlterarSenha(Boolean.TRUE);

            String senhaInicial = cnpj.substring(0, 6);

            cliente.setSenha(MD5Util.cript(senhaInicial));

        }

        if (StringUtils.isNotBlank(u.getBloqueio())) {
            cliente.setStatus(StatusEnum.INATIVO);
        } else {
            cliente.setStatus(StatusEnum.ATIVO);
        }

        cliente.setCnpj(cnpj);

        cliente.setLogin(cnpj);

        cliente.setCodigoSAP(u.getCodigoSAP());

        cliente.setEmailSAP(u.getEmailSAP());

        cliente.setEmail(u.getEmailSAP());

        cliente.setInscricaoEstadual(u.getInscricaoEstadual());

        cliente.setNome(u.getRazaoSocial());

        cliente.setAdministrador(Boolean.FALSE);

        return cliente;

    }


    @Override
    public void excluirPorId(Long id) {

        UsuarioCliente usuarioCliente = this.getDAO().findById(id).orElse(null);

        if (Objects.nonNull(usuarioCliente)) {

            usuarioCliente.setStatus(StatusEnum.INATIVO);

        }

        this.getDAO().save(usuarioCliente);
    }

    private boolean existeUsuario(UsuarioCliente entidade, UsuarioCliente cliente) {
        return (Objects.nonNull(cliente) && Objects.isNull(entidade.getId())) || (Objects.nonNull(cliente) && !entidade.getId().equals(cliente.getId()));
    }

    @Override
    public void preAlterar(UsuarioCliente entidade) {

        UsuarioCliente cliente = this.getDAO().findById(entidade.getId()).orElse(null);

        if (Objects.nonNull(cliente)) {
            entidade.setSenha(cliente.getSenha());
        }

        super.preAlterar(entidade);

        if (existeUsuario(entidade, getDAO().findByEmail(entidade.getEmail()))) {

            throw new ServicoException(MessageSupport.getMessage("MSGE009"));

        }
    }

    @Override
    public Pagina<UsuarioClienteDTO> listarPaginadoDTO(final Paginacao<UsuarioCliente> paginacao) {

        String pesquisa = paginacao.getEntidade().getPesquisa();

        if (StringUtils.isNotBlank(pesquisa)) {

            return this.pageToPagina(findUsuarioClienteByPesquisa(paginacao, pesquisa));

        } else {

            return this.pageToPagina(getDAO().findByAdministradorFalseOrderByIdDesc(paginacao));

        }

    }

    private Page<UsuarioCliente> findUsuarioClienteByPesquisa(Paginacao<UsuarioCliente> paginacao, String pesquisa) {

        StatusEnum status = StatusEnum.contains(pesquisa);

        return getDAO().findAll((root, query, cb) -> {

            List<Predicate> predicates = new ArrayList<>();

            List<Predicate> predicatesOr = new ArrayList<>();

            String like = OperacaoEnum.contains(pesquisa);

            predicates.add(cb.isFalse(root.get("administrador")));

            predicatesOr.add(cb.like(root.get("nome"), like));

            predicatesOr.add(cb.like(root.get("cnpj"), like));

            predicatesOr.add(cb.or(cb.like(root.get("inscricaoEstadual"), like)));

            predicatesOr.add(cb.like(root.get("codigoSAP"), like));

            if (Objects.nonNull(status)) {

                predicatesOr.add(cb.equal(root.get("status"), status));

            }

            Predicate or = cb.or(predicatesOr.toArray(new Predicate[predicatesOr.size()]));

            predicates.add(or);

            query.orderBy(cb.desc(root.get("codigoSAP")));

            return cb.and(predicates.toArray(new Predicate[0]));
        }, paginacao);
    }

    @Override
    public Class<UsuarioClienteDTO> getDTO() {
        return UsuarioClienteDTO.class;
    }

    @Override
    public UsuarioClienteDAO getDAO() {
        return dao;
    }

}
