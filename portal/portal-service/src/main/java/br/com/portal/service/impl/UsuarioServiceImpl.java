package br.com.portal.service.impl;

import br.com.portal.fachada.excecoes.ServicoException;
import br.com.portal.fachada.rest.paginacao.OperacaoEnum;
import br.com.portal.fachada.rest.paginacao.Pagina;
import br.com.portal.fachada.rest.paginacao.Paginacao;
import br.com.portal.fachada.servico.impl.ServicoGenerico;
import br.com.portal.modelo.Enumerator.StatusEnum;
import br.com.portal.modelo.Usuario;
import br.com.portal.modelo.UsuarioCliente;
import br.com.portal.modelo.dto.AlterarPerfilDTO;
import br.com.portal.modelo.dto.ConfiguracaoDTO;
import br.com.portal.modelo.dto.UsuarioDTO;
import br.com.portal.persistencia.UsuarioClienteDAO;
import br.com.portal.persistencia.UsuarioDAO;
import br.com.portal.service.ConfiguracaoService;
import br.com.portal.service.config.PortalEmailConfiguration;
import br.com.portal.service.UsuarioService;
import br.com.portal.util.UtilValidate;
import br.com.portal.util.bundle.MessageSupport;
import br.com.portal.util.encrypt.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import javax.persistence.criteria.Predicate;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class UsuarioServiceImpl extends ServicoGenerico<Long, Usuario> implements UsuarioService {

    private static final String AT = "@";
    private static final String MASK = "*";

    @Autowired
    private UsuarioDAO dao;

    private UsuarioClienteDAO usuarioClienteDAO;

    @Autowired
    private ConfiguracaoService configuracaoService;

    @Autowired
    private PortalEmailConfiguration emailConfiguration;

    @Autowired
    private Environment env;

    @Override
    public void excluirPorId(Long id) {

        Usuario usuario = this.getDAO().findById(id).orElse(null);

        if (Objects.nonNull(usuario)) {

            usuario.setStatus(StatusEnum.INATIVO);

        }

        this.getDAO().save(usuario);
    }

    @Override
    public void preAlterar(Usuario entidade) {

        Usuario usuario = this.getDAO().findById(entidade.getId()).orElse(null);

        entidade.setSenha(usuario.getSenha());

        validarSalvar(entidade);

        super.preAlterar(entidade);
    }

    @Override
    public void preSalvar(Usuario entidade) {

        String senhaInicial = obterSenhaInicial();

        entidade.setSenha(MD5Util.cript(senhaInicial));

        entidade.setAlterarSenha(true);

        super.preSalvar(entidade);

        validarSalvar(entidade);

        enviarSenhaNovoCadastro(entidade, senhaInicial);
    }

    private void validarSalvar(Usuario entidade) {

        Usuario usuario = getDAO().findByLogin(entidade.getLogin());

        if (existeUsuario(entidade, usuario)) {

            throw new ServicoException(MessageSupport.getMessage("MSGE008"));

        }

        usuario = getDAO().findByEmail(entidade.getEmail());

        if (existeUsuario(entidade, usuario)) {

            throw new ServicoException(MessageSupport.getMessage("MSGE009"));

        }
    }

    private boolean existeUsuario(Usuario entidade, Usuario usuario) {
        return (Objects.nonNull(usuario) && Objects.isNull(entidade.getId())) || (Objects.nonNull(usuario) && !entidade.getId().equals(usuario.getId()));
    }

    private String obterSenhaInicial() {

        ConfiguracaoDTO configutacao = ConfiguracaoDTO.getInstance(this.configuracaoService.listar());

        if (Objects.nonNull(configutacao) && StringUtils.isNotBlank(configutacao.getUsuarioSenhaInicial())) {

            return configutacao.getUsuarioSenhaInicial();

        } else {

            return MD5Util.geraNovaSenha();

        }

    }

    @Override
    public Usuario autenticarUsuario(final String login, final String senha) {

        try {

            final String encrypt = MD5Util.cript(senha);

            return dao.findByLoginAndSenhaAndStatus(login, encrypt, StatusEnum.ATIVO);

        } catch (Exception e) {

            throw new ServicoException("Erro ao efetuar login" + login);

        }

    }

    @Override
    public void alterarPerfil(AlterarPerfilDTO entidade) {

        String encrypt = MD5Util.cript(entidade.getSenhaAtual());

        Usuario usuario = dao.findByLoginAndSenhaAndStatus(entidade.getLogin(), encrypt, StatusEnum.ATIVO);

        validarAlterarPerfil(entidade, usuario);

        if (!StringUtils.isBlank(entidade.getNovaSenha())) {

            encrypt = MD5Util.cript(entidade.getNovaSenha());

            usuario.setSenha(encrypt);

            usuario.setAlterarSenha(false);
        }

        if (!StringUtils.isBlank(entidade.getNovoEmail())) {

            usuario.setEmail(entidade.getNovoEmail());

        }

        this.getDAO().save(usuario);
    }

    private void validarAlterarPerfil(AlterarPerfilDTO entidade, Usuario usuario) {

        if (Objects.isNull(usuario)) {
            throw new ServicoException(MessageSupport.getMessage("MSGE002"));
        }

        if (!StringUtils.isBlank(entidade.getNovaSenha()) && (StringUtils.isBlank(entidade.getConfirmarNovaSenha()) || (!entidade.getNovaSenha().equals(entidade.getConfirmarNovaSenha())))) {
            throw new ServicoException(MessageSupport.getMessage("MSGE004"));
        }

        if (!StringUtils.isBlank(entidade.getNovoEmail()) && (StringUtils.isBlank(entidade.getConfirmarNovoEmail()) || (!entidade.getNovoEmail().equals(entidade.getConfirmarNovoEmail())))) {
            throw new ServicoException(MessageSupport.getMessage("MSGE011"));
        }

        if (!StringUtils.isBlank(entidade.getNovaSenha()) && !UtilValidate.isSenhaValida(entidade.getNovaSenha())) {
            throw new ServicoException(MessageSupport.getMessage("MSGE012"));
        }
    }

    @Override
    public String obterEmailBasePorLogin(String login) {

        Usuario usuario = this.dao.findByLoginAndStatus(login, StatusEnum.ATIVO);

        if (Objects.isNull(usuario)) {
            throw new ServicoException(MessageSupport.getMessage("MSGE002"));
        }

        if (!StringUtils.isBlank(usuario.getEmail())) {

            String[] emailSplit = usuario.getEmail().split(AT);

            String sufix = emailSplit[0].substring(3);

            String newSufix = IntStream.range(0, sufix.length()).mapToObj(i -> MASK).collect(Collectors.joining());

            return usuario.getEmail().replace(sufix, newSufix);

        }

        return null;
    }

    @Override
    public Pagina<UsuarioDTO> listarPaginadoDTO(final Paginacao<Usuario> paginacao) {

        String pesquisa = paginacao.getEntidade().getPesquisa();

        if (StringUtils.isNotBlank(pesquisa)) {

            return this.pageToPagina(findUsuarioByPesquisa(paginacao, pesquisa));

        } else {

            return this.pageToPagina(getDAO().findByAdministradorTrueOrderByIdDesc(paginacao));
        }
    }

    private Page<Usuario> findUsuarioByPesquisa(Paginacao<Usuario> paginacao, String pesquisa) {

        StatusEnum status = StatusEnum.contains(pesquisa);

        Long id = StringUtils.isNumeric(pesquisa) ? Long.parseLong(pesquisa) : null;

        return getDAO().findAll((root,query,cb)-> {

            List<Predicate> predicates = new ArrayList<>();

            List<Predicate> predicatesOr = new ArrayList<>();

            String like = OperacaoEnum.contains(pesquisa);

            predicates.add(cb.isTrue(root.get("administrador")));

            predicatesOr.add(cb.like(root.get("nome"), like));

            predicatesOr.add(cb.like(root.get("login"), like));

            predicatesOr.add(cb.or(cb.like(root.get("email"), like)));

            if (Objects.nonNull(status)) {
                predicatesOr.add(cb.equal(root.get("status"), status));
            }

            if(Objects.nonNull(id)){
                predicatesOr.add(cb.equal(root.get("id"), id));
            }

            Predicate or = cb.or(predicatesOr.toArray(new Predicate[predicatesOr.size()]));

            predicates.add(or);

            query.orderBy(cb.desc(root.get("id")));

            return cb.and(predicates.toArray(new Predicate[0]));

        }, paginacao);
    }

    @Override
    public void recuperarSenha(String login, String email) {

        Usuario usuario = this.dao.findByLoginAndStatus(login, StatusEnum.ATIVO);

        validarRecuperarSenha(email, usuario);

        String senhaInicial = null;

        if (usuario.isAdministrador()) {

            senhaInicial = obterSenhaInicial();

        } else   if (usuario instanceof UsuarioCliente){

            UsuarioCliente cliente = (UsuarioCliente) usuario;

            senhaInicial = cliente.getCnpj().substring(0, 6);

        }

        enviarEmailRecuperarSenha(usuario, senhaInicial);

        alterarSenha(usuario, senhaInicial);

    }

    private void alterarSenha(Usuario usuario, String novaSenha) {

        usuario.setAlterarSenha(Boolean.TRUE);

        usuario.setSenha(MD5Util.cript(novaSenha));

        this.getDAO().save(usuario);

    }

    private void validarRecuperarSenha(String email, Usuario usuario) {

        if (Objects.isNull(usuario)) {
            throw new ServicoException(MessageSupport.getMessage("MSGE002"));
        }

        if (!usuario.getEmail().equals(email)) {
            throw new ServicoException(MessageSupport.getMessage("MSGE003"));
        }
    }


    private void enviarEmailRecuperarSenha(Usuario usuario, String senha) {
        enviarEmailSenha(usuario, senha, "/template_email_redefinir_senha.html");
    }

    private void enviarSenhaNovoCadastro(Usuario usuario, String senha) {
        enviarEmailSenha(usuario, senha, "/template_email_cadastro_portal.html");
    }

    private void enviarEmailSenha(Usuario usuario, String senha, String endereco) {
        try {

            JavaMailSender emailSender = emailConfiguration.getJavaMailSender();

            MimeMessage mimeMessage = emailConfiguration.getJavaMailSender().createMimeMessage();

            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "utf-8");

            BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(endereco)));

            String template = reader.lines().collect(Collectors.joining(System.lineSeparator()));

            String url = env.getProperty("email.url.portal");

            template = template.replaceAll("@nome", usuario.getNome())
                    .replaceAll("@usuario", usuario.getLogin())
                    .replaceAll("@senha", senha)
                    .replaceAll("@url-portal", Objects.nonNull(url) ? url : "");

            mimeMessage.setContent(template, "text/html");

            message.setTo(usuario.getEmail());

            message.setSubject("Cadastro Portal");

            emailSender.send(mimeMessage);

        } catch (Exception e) {
            throw new ServicoException(e.getMessage(), usuario);
        }
    }

    @Override
    public Class<UsuarioDTO> getDTO() {
        return UsuarioDTO.class;
    }

    @Override
    public UsuarioDAO getDAO() {
        return dao;
    }

}
