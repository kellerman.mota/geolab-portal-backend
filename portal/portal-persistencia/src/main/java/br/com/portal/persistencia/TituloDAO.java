package br.com.portal.persistencia;

import br.com.portal.fachada.dao.DAO;
import br.com.portal.fachada.rest.paginacao.Paginacao;
import br.com.portal.modelo.Titulo;
import br.com.portal.modelo.UsuarioCliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TituloDAO extends DAO<Long, Titulo> {
    Titulo findByNumeroNotaFiscalAndCliente(String numeroNotaFiscal, UsuarioCliente usuarioCliente);
}
