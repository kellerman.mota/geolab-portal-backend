package br.com.portal.persistencia;

import br.com.portal.fachada.dao.DAO;
import br.com.portal.fachada.rest.paginacao.Paginacao;
import br.com.portal.modelo.Enumerator.StatusEnum;
import br.com.portal.modelo.UsuarioCliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UsuarioClienteDAO extends DAO<Long, UsuarioCliente> {

    Page<UsuarioCliente> findByAdministradorFalseOrderByIdDesc(Pageable pageable);

    UsuarioCliente findByEmail(String email);

	UsuarioCliente findByCodigoSAP(String codigoCliente);

    UsuarioCliente findByCnpj(String cnpj);
}
