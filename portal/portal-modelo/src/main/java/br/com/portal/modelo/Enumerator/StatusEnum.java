package br.com.portal.modelo.Enumerator;

import lombok.Getter;

@Getter
public enum StatusEnum {

    INATIVO("Inativo"),
    ATIVO("Ativo");

    private String descricao;

    StatusEnum(String descricao) {
        this.descricao = descricao;
    }

    public static StatusEnum obter(String pesquisa) {
        if (ATIVO.descricao.contains(pesquisa)) {
            return ATIVO;
        } else if (INATIVO.descricao.contains(pesquisa)) {
            return INATIVO;
        }
        return null;
    }

    public static StatusEnum contains(String pesquisa) {
        if (ATIVO.descricao.toUpperCase().contains(pesquisa.toUpperCase())) {
            return ATIVO;
        } else if (INATIVO.descricao.toUpperCase().contains(pesquisa.toUpperCase())) {
            return INATIVO;
        }
        return null;
    }

    @Override
    public String toString() {
        return this.getDescricao();
    }
}
