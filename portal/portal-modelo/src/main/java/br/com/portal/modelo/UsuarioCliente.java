package br.com.portal.modelo;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@PrimaryKeyJoinColumn(name = "id_usuario", foreignKey = @ForeignKey(name = "fk_usuario_cliente"))
@Table(name = "tb_usuario_cliente")
public class UsuarioCliente extends Usuario {

    @Column(name = "codigo_sap")
    @NotNull(message = "codigosap.obrigatorio")
    private String codigoSAP;

    @Column(name = "email_sap")
    @NotNull(message = "email.obrigatorio")
    private String emailSAP;

    @Column(name = "cnpj")
    @NotNull(message = "cnpj.obrigatorio")
    @CNPJ(message = "cnpj.invalido")
    private String cnpj;

    @Column(name = "inscricao_estadual")
    private String inscricaoEstadual;

    @Column(name = "dt_ultimo_acesso")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataUltimoAcesso;

}
