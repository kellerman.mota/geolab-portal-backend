package br.com.portal.modelo.Enumerator;

import lombok.Getter;

@Getter
public enum StatusTituloEnum {

    EM_ABERTO("Em Aberto"),
    VENCIDO("Vencido");

    private String descricao;

    StatusTituloEnum(String descricao) {
        this.descricao = descricao;
    }

    public static StatusTituloEnum obterPorDescricao(String pesquisa) {
        if (EM_ABERTO.descricao.equalsIgnoreCase(pesquisa)) {
            return EM_ABERTO;
        } else if (VENCIDO.descricao.equalsIgnoreCase(pesquisa)) {
            return VENCIDO;
        }
        return null;
    }

    public static StatusTituloEnum obter(String pesquisa) {
        if (EM_ABERTO.name().equalsIgnoreCase(pesquisa)) {
            return EM_ABERTO;
        } else if (VENCIDO.name().equalsIgnoreCase(pesquisa)) {
            return VENCIDO;
        }
        return null;
    }

    @Override
    public String toString() {
        return this.getDescricao();
    }

}
