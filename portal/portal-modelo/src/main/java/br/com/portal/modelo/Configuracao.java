package br.com.portal.modelo;

import br.com.portal.modelo.Enumerator.CodigoConfiguracaoEnum;
import br.com.portal.modelo.base.IEntidade;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "tb_configuracao")
public class Configuracao implements IEntidade {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "codigo")
    @Enumerated(EnumType.STRING)
    private CodigoConfiguracaoEnum codigo;

    @Column(name = "valor")
    private String valor;

    public Configuracao() {
    }

    public Configuracao(CodigoConfiguracaoEnum codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }
}
