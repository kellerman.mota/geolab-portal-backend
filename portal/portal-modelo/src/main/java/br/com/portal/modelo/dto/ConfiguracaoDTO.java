package br.com.portal.modelo.dto;

import br.com.portal.modelo.Enumerator.CodigoConfiguracaoEnum;
import br.com.portal.modelo.Configuracao;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConfiguracaoDTO {

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.CONFIGURACAO_EMAIL_SERVIDOR)
    private String emailServidor;

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.CONFIGURACAO_EMAIL_EMAIL)
    private String emailEmail;

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.CONFIGURACAO_EMAIL_PORTA)
    private String emailPorta;

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.CONFIGURACAO_EMAIL_PROTOCOLO)
    private String emailProtocolo;

    @ConfiguracaoReferencia(value = CodigoConfiguracaoEnum.CONFIGURACAO_EMAIL_SENHA, isSenha = true)
    private String emailSenha;

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.CONFIGURACAO_CONTATO_TELEFONE)
    private String contatoTelefone;

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.CONFIGURACAO_CONTATO_EMAIL)
    private String contatoEmail;

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.CONFIGURACAO_SAP_USUARIO)
    private String sapUsuario;

    @ConfiguracaoReferencia(value = CodigoConfiguracaoEnum.CONFIGURACAO_SAP_SENHA, isSenha = true)
    private String sapSenha;

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.CONFIGURACAO_DIRETORIO_CAMINHO)
    private String diretorioCaminho;

    @ConfiguracaoReferencia(CodigoConfiguracaoEnum.USUARIO_SENHA_INICIAL)
    private String usuarioSenhaInicial;

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface ConfiguracaoReferencia {
        CodigoConfiguracaoEnum value();

        boolean isSenha() default false;
    }

    public static ConfiguracaoDTO getInstance(final List<Configuracao> configuracaos) {
        if (Objects.nonNull(configuracaos) && !configuracaos.isEmpty()) {
            final ConfiguracaoDTO dto = new ConfiguracaoDTO();
            Field[] fields = dto.getClass().getDeclaredFields();
              configuracaos.forEach((configuracao) -> {
                for (Field field : fields) {
                    if (field.isAnnotationPresent(ConfiguracaoReferencia.class)) {
                        ConfiguracaoReferencia anotacao = field.getAnnotation(ConfiguracaoReferencia.class);
                        if (configuracao.getCodigo() == anotacao.value()) {
                            field.setAccessible(true);
                            try {
                                String valor = configuracao.getValor();
                               // if (anotacao.isSenha()) {
                                 //   valor = null;
                                //}
                                field.set(dto, valor);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
            return dto;
        }
        return null;
    }

}
