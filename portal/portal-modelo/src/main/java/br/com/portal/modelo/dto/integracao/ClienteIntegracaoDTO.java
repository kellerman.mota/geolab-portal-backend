package br.com.portal.modelo.dto.integracao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "CLIENTE")
public class ClienteIntegracaoDTO {

    @JsonProperty("COD_CLIENTE")
    private String codigoSAP;

    @JsonProperty("EMAIL")
    private String emailSAP;

    @JsonProperty("CNPJ")
    private String cnpj;

    @JsonProperty("INCRICAO_ESTADUAL")
    private String inscricaoEstadual;

    @JsonProperty("RAZAO_SOCIAL")
    private String razaoSocial;

    @JsonProperty("BLOQUEIO")
    private String bloqueio;


}
