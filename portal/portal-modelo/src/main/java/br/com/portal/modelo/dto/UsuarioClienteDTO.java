package br.com.portal.modelo.dto;

import br.com.portal.modelo.Usuario;
import br.com.portal.modelo.UsuarioCliente;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioClienteDTO extends UsuarioDTO {

    private String codigoSAP;

    private String emailSAP;

    private String cnpj;

    private String inscricaoEstadual;

    private Date dataUltimoAcesso;

    public UsuarioClienteDTO(final UsuarioCliente usuarioCliente) {

        super(usuarioCliente);

        setCodigoSAP(usuarioCliente.getCodigoSAP());

        setEmailSAP(usuarioCliente.getEmailSAP());

        setCnpj(usuarioCliente.getCnpj());

        setInscricaoEstadual(usuarioCliente.getInscricaoEstadual());

        setDataUltimoAcesso(usuarioCliente.getDataUltimoAcesso());
    }

}
