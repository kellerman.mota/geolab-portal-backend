package br.com.portal.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AlterarPerfilDTO extends DataDTO{

	private String senhaAtual;

	private String login;

	private String novaSenha;

	private String confirmarNovaSenha;

	private String novoEmail;

	private String confirmarNovoEmail;

}
