package br.com.portal.modelo.base;

import java.io.Serializable;

public interface IEntidade extends Serializable {

    Long getId();

}
