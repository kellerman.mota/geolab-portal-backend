package br.com.portal.fachada.rest.paginacao;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort.Direction;

@NoArgsConstructor
@AllArgsConstructor
public class Ordenacao implements Serializable {

	private static final long serialVersionUID = 1L;

	private String campo;

    private Direction ordenacao;

    public String getCampo() {

        return this.campo;
    }

    public void setCampo(final String campo) {

        this.campo = campo;
    }

    public Direction getOrdenacao() {

        return this.ordenacao;
    }

    public void setOrdenacao(final Direction ordenacao) {

        this.ordenacao = ordenacao;
    }
}