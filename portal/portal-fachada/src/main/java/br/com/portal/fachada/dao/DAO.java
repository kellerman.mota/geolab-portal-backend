package br.com.portal.fachada.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DAO<ID extends Serializable, E extends Serializable> extends JpaRepository<E, ID>, JpaSpecificationExecutor<E> {

}
