package br.com.portal.fachada.excecoes;

public class EndpointException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private final Object[] argumentos;

    public EndpointException(final String mensagem, final Object... argumentos) {

        super(mensagem);

        this.argumentos = argumentos;
    }

    public Object[] getArgumentos() {

        return this.argumentos.clone();
    }
}
