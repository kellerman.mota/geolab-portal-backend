package br.com.portal.fachada.rest.paginacao;

import java.io.Serializable;

public class Filtro implements Serializable {

	private static final long serialVersionUID = 1L;

	private String campo;
	
	private OperacaoEnum operacao;

    public String getCampo() {

        return this.campo;
    }

    public void setCampo(final String campo) {

        this.campo = campo;
    }

	public OperacaoEnum getOperacao() {
		
		return operacao;
		
	}

	public void setOperacao(OperacaoEnum operacao) {
		
		this.operacao = operacao;
		
	}

}
