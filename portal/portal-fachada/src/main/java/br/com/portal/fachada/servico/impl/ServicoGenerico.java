package br.com.portal.fachada.servico.impl;

import br.com.portal.fachada.excecoes.ServicoException;
import br.com.portal.fachada.rest.paginacao.Filtro;
import br.com.portal.fachada.rest.paginacao.Pagina;
import br.com.portal.fachada.rest.paginacao.Paginacao;
import br.com.portal.fachada.service.Servico;
import br.com.portal.modelo.base.IEntidade;
import br.com.portal.modelo.dto.DataDTO;
import br.com.portal.util.bundle.MessageSupport;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.data.domain.Page;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class ServicoGenerico<ID extends Serializable, E extends IEntidade> implements Servico<ID, E > {

    @Override
    public E get(ID id) {

        return this.getDAO().findById(id).get();

    }

    @Override
    public void salvar(List<E> lista) {

        lista.stream().forEach(e -> this.salvar(e));

    }

    @Override
    public E salvar(E entidade) {

        this.preSalvar(entidade);

        return this.getDAO().save(entidade);
    }

    @Override
    public E alterar(E entidade) {

        this.preAlterar(entidade);

        return this.getDAO().save(entidade);
    }

    @Override
    public void excluir(E entidade) {

        preRemover(entidade);

        this.getDAO().delete(entidade);
    }

    @Override
    public void excluirPorId(ID id) {

        preRemover(this.getDAO().findById(id).orElse(null));

        this.getDAO().deleteById(id);
    }

    @Override
    public List<E> listar() {

        return this.getDAO().findAll();

    }

    @Override
    public Pagina<? extends DataDTO> listarPaginadoDTO(final Paginacao<E> paginacao) {

        final Example<E> restricoes = this.obterRestricoes(paginacao);

        if (restricoes != null) {

            return this.pageToPagina(this.getDAO().findAll(restricoes, paginacao));

        } else {

            return this.pageToPagina(this.getDAO().findAll(paginacao));

        }

    }

    protected Pagina pageToPagina(final Page<E> page) {

        final ModelMapper modelMapper = new ModelMapper();

        final Collection<DataDTO> collect = page.getContent()

                .stream()

                .map(e -> modelMapper.map(e, this.getDTO()))

                .collect(Collectors.toList());

            final Pagina<DataDTO> pagina = new Pagina<>();

        pagina.setContent(collect);

        pagina.setTotalElements(page.getTotalElements());

        return pagina;

    }

    @Override
    public Example<E> obterRestricoes(final Paginacao<E> paginacao) {

        if (paginacao == null || paginacao.getEntidade() == null) {

            return null;

        }

        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withIgnoreNullValues()
                .withIgnoreCase()
                .withStringMatcher(StringMatcher.CONTAINING);

        for (Filtro filtro : paginacao.getFiltros()) {
            matcher = matcher.withMatcher(filtro.getCampo(), filtro.getOperacao().obterOperacao());
        }
        final Example<E> example = Example.of(paginacao.getEntidade(), matcher);

        return example;
    }

    @Override
    public Long count() {

        return this.getDAO().count();

    }

    public void preSalvar(E entidade) {

        validarSalvarAlterar(entidade);

    }

    public void preAlterar(E entidade) {

        validarSalvarAlterar((E) entidade);

    }

    protected void validarSalvarAlterar(E entidade) {

        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

        final Validator validator = factory.getValidator();

        final Set<ConstraintViolation<E>> validate = validator.validate(entidade);

        if (validate.size() > 0) {

            throw new ServicoException(MessageSupport.getMessage(validate.iterator().next().getMessage()));

        }
    }

    public void preRemover(E entidade) {

    }

    @Override
    public Class<? extends DataDTO> getDTO() {

        throw new ServicoException("Método não implementado.!");

    }

}