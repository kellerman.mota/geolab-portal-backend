package br.com.portal.fachada.rest;

import br.com.portal.modelo.base.IEntidade;
import br.com.portal.util.rest.MapBuilder;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public abstract class ManutencaoEndpoint<E extends IEntidade> extends ConsultaEndpoint<E> {

	private static final long serialVersionUID = 1L;

	@POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public Response salvar(E entidade) {

		this.getServico().salvar(entidade);

		return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSGS001")).build()).build();

	}

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response alterar(E entidade) {

        this.getServico().alterar(entidade);

        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSGS001")).build()).build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response excluir(@PathParam("id") Long id) {

        this.getServico().excluirPorId(id);

        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSGS001")).build()).build();
    }
    

}
