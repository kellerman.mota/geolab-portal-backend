package br.com.portal.fachada.rest.paginacao;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Getter
@Setter
public class Paginacao<E extends Serializable> implements Pageable {

	private int pageSize;

    private int pageNumber;
    
    private int offset;
    
    private E entidade;

    private List<Filtro> filtros;
    
    private Ordenacao ordenacao;

	@Override
	public int getPageNumber() {
		return pageNumber;
	}

	@Override
	public int getPageSize() {
		return pageSize;
	}

	@Override
	public long getOffset() {
		return offset;
	}

	@Override
	public Sort getSort() {
		
		if (ordenacao == null) {
			
			return new Sort(Direction.DESC, "dataCadastro");
			
		}
		
		return new Sort(ordenacao.getOrdenacao(), ordenacao.getCampo());
	}

	@Override
	public Pageable next() {
		return null;
	}

	@Override
	public Pageable previousOrFirst() {
		return null;
	}

	@Override
	public Pageable first() {
		return null;
	}

	@Override
	public boolean hasPrevious() {
		return false;
	}

	public List<Filtro> getFiltros() {
		if(filtros == null) {
			filtros = new ArrayList<>();
		}
		return filtros;
	}
}