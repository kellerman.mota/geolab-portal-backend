package  br.com.portal.rest.mapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class MapperUtils {

    public static Response create(final AppResponse response) {

        return MapperUtils.create(Response.Status.CONFLICT, response);
    }

    public static Response create(final Response.Status status, final AppResponse response) {

        return Response.status(status).entity(response).type(MediaType.APPLICATION_JSON).build();
    }

    public static Response create(final String mensagem) {

        return MapperUtils.create(Response.Status.CONFLICT, mensagem);
    }

    public static Response create(final Response.Status status, final String mensagem) {

        return Response.status(status).entity(AppResponse.create(mensagem)).type(MediaType.APPLICATION_JSON).build();
    }

    public static Response create(final int status, final String mensagem) {

        return create(status, AppResponse.create(mensagem));
    }

    public static Response create(final int status, final AppResponse response) {

        return Response.status(status).entity(response).type(MediaType.APPLICATION_JSON).build();
    }
}