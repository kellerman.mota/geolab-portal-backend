package br.com.portal.rest.mapper;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AppResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String mensagem;

    private AppResponse(String mensagem) {

        this.mensagem = mensagem;
    }

    public static AppResponse create(String mensagem) {

        return new AppResponse(mensagem);
    }

    public String getMensagem() {

        return mensagem;
    }

    public void setMensagem(String mensagem) {

        this.mensagem = mensagem;
    }
}