package br.com.portal.rest.endpoint;

import br.com.portal.fachada.rest.ManutencaoEndpoint;
import br.com.portal.modelo.Usuario;
import br.com.portal.modelo.dto.AlterarPerfilDTO;
import br.com.portal.service.UsuarioService;
import br.com.portal.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.function.Function;

@Component
@Path("administrador")
public class UsuarioAdministradorEndpoint extends ManutencaoEndpoint<Usuario> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private UsuarioService usuarioService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("alterarPerfil")
	public Response alterarPerfil(final AlterarPerfilDTO entidade) {

		this.getServico().alterarPerfil(entidade);

		return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSGS001")).build()).build();
	}

	@Override
	protected Function<? super Usuario, ? extends String> toStringItemSelectIem() {
		return u -> u.getNome();
	}

	@Override
	protected UsuarioService getServico() {
		return usuarioService;
	}
}
