package br.com.portal.rest.security;

import br.com.portal.fachada.rest.BaseEndpoint;
import br.com.portal.rest.contexto.ContextoManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthFilter implements ContainerRequestFilter {

    @Autowired
    private ContextoManager contextoManager;
    
    @Context
    protected HttpServletRequest request;

    @Context
    protected ResourceInfo info;

    public void filter(final ContainerRequestContext containerRequestContext) throws IOException {

        if (!this.anotacaoEstaPresente(PermitAll.class) && !containerRequestContext.getMethod().equalsIgnoreCase("OPTIONS")) {

            if (this.anotacaoEstaPresente(DenyAll.class)) {

                throw new NotAuthorizedException("Nobody can access this resource");
            }

            final String token = containerRequestContext.getHeaderString(BaseEndpoint.AUTHORIZATION_PROPERTY);

            if (token == null || !this.validarToken(token)) {

                throw new NotAuthorizedException("Access denied for this resource");
            }

            if (this.anotacaoEstaPresente(RolesAllowed.class)) {

                final Set<String> roles = new HashSet<String>(Arrays.asList(this.obterAnotacao(RolesAllowed.class).value()));

                if (!this.isUserAllowed(token, roles)) {

                    throw new NotAuthorizedException("Access denied for this resource");
                }
            }

            this.request.setAttribute(BaseEndpoint.TOKEN_USER_AUTHORIZATION, token);

            this.request.setAttribute(BaseEndpoint.TOKEN_USER, this.getUsuarioEndpoint(token));
        }
        
    }

    protected boolean anotacaoEstaPresente(final Class<? extends Annotation> annotationClass) {

        return this.info.getResourceClass().isAnnotationPresent(annotationClass) || this.info.getResourceMethod().isAnnotationPresent(annotationClass);
    }

    protected <T extends Annotation> T obterAnotacao(final Class<T> annotationClass) {

        return (this.info.getResourceMethod().isAnnotationPresent(annotationClass)) ? this.info.getResourceMethod().getAnnotation(annotationClass) : this.info.getResourceClass().getAnnotation(annotationClass);
    }

    protected Object getUsuarioEndpoint(String token) {

        return this.contextoManager.obter(token);
    }

    protected boolean validarToken(String token) {

        return this.contextoManager.existe(token);
    }

    protected boolean isUserAllowed(String token, Set<String> perfis) {

        this.contextoManager.obter(token);

        return true;
    }
}
