package  br.com.portal.rest.mapper;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InternalServerErrorExceptionMapper implements ExceptionMapper<InternalServerErrorException> {

    public Response toResponse(InternalServerErrorException exception) {

        return MapperUtils.create(Response.Status.INTERNAL_SERVER_ERROR, exception.getMessage());
    }
}
