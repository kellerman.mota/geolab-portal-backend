package  br.com.portal.rest.mapper;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotAcceptableExceptionMapper implements ExceptionMapper<NotAcceptableException> {

    public Response toResponse(NotAcceptableException e) {

        return MapperUtils.create(Response.Status.NOT_ACCEPTABLE, e.getMessage());
    }
}