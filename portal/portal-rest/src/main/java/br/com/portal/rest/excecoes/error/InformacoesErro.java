package br.com.portal.rest.excecoes.error;

public class InformacoesErro {

    public final String url;

    public final String stacktrace;

    public final String codigoErro;

    public final Throwable exception;

    public InformacoesErro(String url, Throwable ex, String codigoErro) {

        this.url = url;

        this.stacktrace = ex.getLocalizedMessage();

        this.exception = ex;

        this.codigoErro = codigoErro;
    }

    public String getUrl() {

        return url;
    }

    public String getStacktrace() {

        return stacktrace;
    }

    public String getCodigoErro() {

        return codigoErro;
    }

    public Throwable getException() {

        return exception;
    }
}
