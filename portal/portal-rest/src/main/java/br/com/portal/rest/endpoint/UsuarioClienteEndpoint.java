package br.com.portal.rest.endpoint;

import br.com.portal.fachada.rest.ManutencaoEndpoint;
import br.com.portal.modelo.UsuarioCliente;
import br.com.portal.modelo.dto.integracao.ClienteIntegracaoDTO;
import br.com.portal.modelo.dto.integracao.MensagemIntegracaoDTO;
import br.com.portal.service.UsuarioClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.function.Function;

@Component
@Path("cliente")
public class UsuarioClienteEndpoint extends ManutencaoEndpoint<UsuarioCliente> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private UsuarioClienteService usuarioClienteService;

    @POST
    @PermitAll
    @Path("salvarClientes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<MensagemIntegracaoDTO> salvarClientes(List<ClienteIntegracaoDTO> clientes) {
        return usuarioClienteService.salvarClientes(clientes);
    }

    @Override
    protected Function<? super UsuarioCliente, ? extends String> toStringItemSelectIem() {
        return u -> u.getNome();
    }

    @Override
    protected UsuarioClienteService getServico() {
        return usuarioClienteService;
    }
}
