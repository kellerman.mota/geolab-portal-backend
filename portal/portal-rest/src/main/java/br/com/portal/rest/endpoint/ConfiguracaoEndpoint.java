package br.com.portal.rest.endpoint;

import br.com.portal.fachada.excecoes.ServicoException;
import br.com.portal.fachada.rest.ManutencaoEndpoint;
import br.com.portal.modelo.Configuracao;
import br.com.portal.modelo.dto.ConfiguracaoDTO;
import br.com.portal.service.ConfiguracaoService;
import br.com.portal.util.bundle.MessageSupport;
import br.com.portal.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@Component
@Path("configuracao")
public class ConfiguracaoEndpoint extends ManutencaoEndpoint<Configuracao> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ConfiguracaoService configuracaoService;

    @POST
    @PermitAll
    @Path("/obterConfiguracoes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ConfiguracaoDTO obterConfiguracoes() {
        return ConfiguracaoDTO.getInstance(this.getServico().listar());
    }


    @POST
    @Path("/salvarConfiguracoes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarConfiguracoes(ConfiguracaoDTO entidade) {

        this.getServico().salvar(preencherParamentros(entidade));

        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSGS001")).build()).build();

    }

    private List<Configuracao> preencherParamentros(ConfiguracaoDTO entidade) {

        List<Configuracao> configuracoes = new ArrayList<>();

        if (Objects.nonNull(entidade)) {

            Field[] fields = entidade.getClass().getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);
                try {
                    Object value = field.get(entidade);
                    if (Objects.nonNull(value)) {
                        ConfiguracaoDTO.ConfiguracaoReferencia anotacao = field.getAnnotation(ConfiguracaoDTO.ConfiguracaoReferencia.class);
                        String valor = (String) value;
                        Configuracao configuracao = new Configuracao();
                        configuracao.setCodigo(anotacao.value());
                        configuracao.setValor(valor);
                        configuracoes.add(configuracao);
                    }

                } catch (IllegalAccessException e) {
                   throw new ServicoException(MessageSupport.getMessage("erro.salvar.configuracao"));
                }
            }

        }
        return configuracoes;
    }


    @Override
    protected Function<? super Configuracao, ? extends String> toStringItemSelectIem() {
        return u -> u.getValor();
    }

    @Override
    protected ConfiguracaoService getServico() {
        return configuracaoService;
    }
}
