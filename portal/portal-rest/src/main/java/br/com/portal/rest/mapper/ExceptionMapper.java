package  br.com.portal.rest.mapper;

import java.util.ResourceBundle;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception exception) {
    	
    	exception.printStackTrace();

        return MapperUtils.create(Response.Status.fromStatusCode(412), ResourceBundle.getBundle("message").getString("MSG004"));
    }
}
