package br.com.portal.rest.contexto;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContextoManager {

    private static final String MANAGER_CONTEXT = "MANAGER.CONTEXT.APP";

    @Autowired
    private ServletContext servletContext;

    @PostConstruct
    public void init() {
    	
        this.servletContext.setAttribute(MANAGER_CONTEXT, new HashMap<String, Object>());
    }

    @SuppressWarnings("unchecked")
	public void armazenar(String chave, Object valor) {

        ((Map<String, Object>) this.servletContext.getAttribute(MANAGER_CONTEXT)).put(chave, valor);
    }

    @SuppressWarnings("unchecked")
	public <T> T obter(String chave) {

        return (T) ((Map<String, Object>) this.servletContext.getAttribute(MANAGER_CONTEXT)).get(chave);
    }

    @SuppressWarnings("unchecked")
	public void remover(String chave) {

        ((Map<String, Object>) this.servletContext.getAttribute(MANAGER_CONTEXT)).remove(chave);
    }

    @SuppressWarnings("unchecked")
	public boolean existe(String chave) {

        return ((Map<String, Object>) this.servletContext.getAttribute(MANAGER_CONTEXT)).containsKey(chave);
    }
}
